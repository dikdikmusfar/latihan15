<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FilmController extends Controller
{
    public function create(){
        return view('films.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:films',
            'ringkasan' => 'required|unique:films',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);
        $query = DB::table('films')->insert([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"],
            "poster" => $request["poster"],
            "genre_id" => $request["genre_id"]
        ]);
        return redirect('/films')->with('success', 'Genre berhasil ditambah!');
        }

    public function index(){
        $films = DB::table('films')->get(); //Select * from films
        //dd($films);
         return view('films.index', compact('films'));
    }

    public function show($id){
        $film = DB::table('films')->where('id',$id)->first();
        //dd($film);
        return view('films.show', compact('film'));
    }

    public function edit($id){
        $film = DB::table('films')->where('id',$id)->first();
        return view('films.edit', compact('film'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:films',
            'ringkasan' => 'required|unique:films',
            'tahun' => 'required',
            'poster' => 'required',
            'genre_id' => 'required'
        ]);

        $query = DB::table('films')
        -> where('id',$id)
        -> update([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"],
            "poster" => $request["poster"],
            "genre_id" => $request["genre_id"]
        ]);
        return redirect('/films')->with('success', 'Berhasil update film!');
    }

    public function destroy($id) {
        $query = DB::table('films')->where('id',$id)->delete();
        return redirect('/films')->with('success', 'Genre berhasil di hapus!');
    }
}
