<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GenreController extends Controller
{
    public function create(){
        return view('genres.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'nama' => 'required|unique:genres'
        ]);
        $query = DB::table('genres')->insert([
            "nama" => $request["nama"],
        ]);
        return redirect('/genres')->with('success', 'Genre berhasil ditambah!');
        }

        public function index(){
            $genres = DB::table('genres')->get(); //Select * from genres
            //dd($genres);
            return view('genres.index', compact('genres'));
        }

        public function show($id){
            $genre = DB::table('genres')->where('id',$id)->first();
            //dd($genre);
            return view('genres.show', compact('genre'));
        }

        public function edit($id){
            $genre = DB::table('genres')->where('id',$id)->first();
            return view('genres.edit', compact('genre'));
        }

        public function update($id, Request $request){
            $request->validate([
                'nama' => 'required|unique:genres'
            ]);

            $query = DB::table('genres')
            -> where('id',$id)
            -> update([
                'nama' => $request['nama']
            ]);
            return redirect('/genres')->with('success', 'Berhasil update genre!');
        }

        public function destroy($id) {
            $query = DB::table('genres')->where('id',$id)->delete();
            return redirect('/genres')->with('success', 'Genre berhasil di hapus!');
        }
}
