@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Edit a Film {{$film->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/films/{{$film->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputJudul1">Judul</label>
                    <input type="Judul" class="form-control" id="exampleInputJudul1" name="judul" value=" {{ old('judul', $film->judul)}}" placeholder="Enter Judul">
                        @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                <div>
                  <div class="form-group">
                    <label for="exampleInputRingkasan1">Ringkasan</label>
                    <input type="Ringkasan" class="form-control" id="exampleInputRingkasan1" name="ringkasan" value=" {{ old('ringkasan', $film->ringkasan)}}" placeholder="Enter Ringkasan">
                        @error('ringkasan')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputTahun1">Tahun</label>
                    <input type="Tahun" class="form-control" id="exampleInputTahun1" name="tahun" value=" {{ old('tahun', $film->tahun)}}" placeholder="Tahun">
                        @error('tahun')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPoster1">Poster</label>
                    <input type="Poster" class="form-control" id="exampleInputPoster1" name="poster" value=" {{ old('poster', $film->poster)}}" placeholder="Poster">
                        @error('poster')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputGenreID1">Genre ID</label>
                    <input type="Genre ID" class="form-control" id="exampleInpuGenreID1" name="genre_id" value=" {{ old('genre_id', $film->genre_id)}}" placeholder="Genre ID">
                        @error('genre_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
</div>
@endsection