@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Films Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
              <a class="btn btn-primary" href="/films/create">Create a New Film</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">Number</th>
                      <th>Judul</th>
                      <th>Ringkasan</th>
                      <th style="width: 40px">Tahun</th>
                      <th>Poster</th>
                      <th>Genre ID</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($films as $key => $film)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $film->judul }}</td>
                    <td>{{ $film->ringkasan }}</td>
                    <td>{{ $film->tahun }}</td>
                    <td>{{ $film->poster }}</td>
                    <td>{{ $film->genre_id }}</td>
                    <td style="display: flex;">
                    <a href="/films/{{$film->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/films/{{$film->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/films/{{$film->id}}" method="post">
                    @csrf
                    @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="6" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@endsection