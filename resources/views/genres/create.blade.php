@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Create a New Genre</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/genres" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="Nama" class="form-control" id="nama" value=" {{ old('nama', '')}}" name="nama" placeholder="Enter Nama">
                        @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
</div>
@endsection