@extends('adminlte.master')

@section('content')
<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Edit a Genre {{$genre->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/genres/{{$genre->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="Nama" class="form-control" id="nama" value=" {{ old('nama', $genre->nama)}}" name="nama" placeholder="Enter Nama">
                        @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
</div>
@endsection