@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Genres Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
              <a class="btn btn-primary" href="/genres/create">Create a New Genre</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">Number</th>
                      <th>Nama</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($genres as $key => $genre)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $genre->nama }}</td>
                    <td style="display: flex;">
                    <a href="/genres/{{$genre->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/genres/{{$genre->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/genres/{{$genre->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                    </td>
                    </tr>
                    @empty
                        <tr>
                            <td colspan="2" align="center"> No Data Recorded </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@endsection