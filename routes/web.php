<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::get('/form', 'AuthController@form');

Route::post('/welcome', 'AuthController@home_post');

Route::get('/master',function(){
    return view('/adminlte/master');
});

Route::get('/items',function(){
    return view('items/index');
});

Route::get('/items/create',function(){
    return view('items/create');
});

Route::get('/table', 'TableController@table');

Route::get('/data-table', 'TableController@data_table');

Route::get('/users/create', 'UserController@create');

Route::get('/films/create', 'FilmController@create');
Route::post('/films', 'FilmController@store');
Route::get('/films', 'FilmController@index');
Route::get('/films/{id}', 'FilmController@show');
Route::get('/films/{id}/edit', 'FilmController@edit');
Route::put('/films/{id}', 'FilmController@update');
Route::delete('/films/{id}', 'FilmController@destroy');

Route::get('/genres/create', 'GenreController@create');
Route::post('/genres', 'GenreController@store');
Route::get('/genres', 'GenreController@index');
Route::get('/genres/{id}', 'GenreController@show');
Route::get('/genres/{id}/edit','GenreController@edit');
Route::put('/genres/{id}','GenreController@update');
Route::delete('/genres/{id}','GenreController@destroy');